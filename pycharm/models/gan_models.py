from tensorflow.keras import layers
import tensorflow as tf

from util.manager_gan import create_manager


def prepare_gan(generator, discriminator, lr_gen, lr_dis, checkpoint_prefix):
    generator_optimizer = tf.keras.optimizers.Adam(lr_gen)
    discriminator_optimizer = tf.keras.optimizers.Adam(lr_dis)
    manager = create_manager(generator, discriminator, generator_optimizer, discriminator_optimizer, checkpoint_prefix)
    return manager, generator_optimizer, discriminator_optimizer


def load_generator_weights(model):
    load_checkpoint_filepath = './resources/kocka_mp4/GAN/generator/gen_4'
    model.load_weights(load_checkpoint_filepath)
    return model


def load_discriminator_weights(model):
    load_checkpoint_filepath = '.resources/kocka_mp4/GAN/discriminator/disc'
    model.load_weights(load_checkpoint_filepath)
    return model


def make_generator_model(input_shape):
    model = tf.keras.Sequential()
    model.add(layers.Input(input_shape))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.0))
    model.add(layers.Conv2D(96, (5, 5), activation='relu', padding='same',
                            kernel_constraint=tf.keras.constraints.MaxNorm(20)))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.0))
    model.add(
        layers.Conv2D(48, 3, activation='relu', padding='same', kernel_constraint=tf.keras.constraints.MaxNorm(20)))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.0))
    model.add(
        layers.Conv2D(24, 3, activation='relu', padding='same', kernel_constraint=tf.keras.constraints.MaxNorm(20)))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.0))
    model.add(
        layers.Conv2D(12, 3, activation='relu', padding='same', kernel_constraint=tf.keras.constraints.MaxNorm(20)))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.0))
    model.add(
        layers.Conv2D(input_shape[2] / 2, (5, 5), padding='same', kernel_constraint=tf.keras.constraints.MaxNorm(20)))
    return model


def make_discriminator_model(input_shape):
    model = tf.keras.Sequential()
    model.add(layers.Input(input_shape))
    model.add(layers.Conv2D(24, (5, 5), strides=(2, 2), activation='relu', padding='same'))
    model.add(layers.Dropout(0.3))
    model.add(layers.Conv2D(48, (5, 5), strides=(2, 2), activation='relu', padding='same'))
    model.add(layers.Dropout(0.3))
    model.add(layers.MaxPooling2D(pool_size=2))
    model.add(layers.Conv2D(96, (5, 5), strides=(2, 2), activation='relu', padding='same'))
    model.add(layers.Dropout(0.3))

    model.add(layers.Flatten())
    model.add(layers.Dense(1, activation='sigmoid'))

    return model

from tensorflow.keras import layers
from tensorflow.keras import models

def load_unet_weights(model):
    load_checkpoint_filepath = './resources/kocka_mp4/U_Net/u_net_4'
    model.load_weights(load_checkpoint_filepath)
    return model


def U_Net_model(input_shape):
  input = layers.Input(input_shape)
  return U_Net_defined_input(input_shape, input)

def U_Net_defined_input(input_shape, input):
  convblock_1 = layers.Conv2D(32, 3, activation='relu', padding='same')(input)
  convblock_1 = layers.BatchNormalization()(convblock_1)
  convblock_1 = layers.Conv2D(32, 3, activation='relu', padding='same')(convblock_1)
  convblock_1 = layers.BatchNormalization()(convblock_1)
  convblock_1_2 = layers.MaxPooling2D(pool_size=2)(convblock_1)

  convblock_2 = layers.Conv2D(64, 3, activation='relu', padding='same')(convblock_1_2)
  convblock_2 = layers.BatchNormalization()(convblock_2)
  convblock_2 = layers.Conv2D(64, 3, activation='relu', padding='same')(convblock_2)
  convblock_2 = layers.BatchNormalization()(convblock_2)
  convblock_2_3 = layers.MaxPooling2D(pool_size=2)(convblock_2)

  convblock_3 = layers.Conv2D(128, 3, activation='relu', padding='same')(convblock_2_3)
  convblock_3 = layers.BatchNormalization()(convblock_3)
  convblock_3 = layers.Conv2D(128, 3, activation='relu', padding='same')(convblock_3)
  convblock_3 = layers.BatchNormalization()(convblock_3)
  convblock_3_4 = layers.MaxPooling2D(pool_size=2)(convblock_3)

  convblock_4 = layers.Conv2D(256, 3, activation='relu', padding='same')(convblock_3_4)
  convblock_4 = layers.BatchNormalization()(convblock_4)
  convblock_4 = layers.Conv2D(256, 3, activation='relu', padding='same')(convblock_4)
  convblock_4 = layers.BatchNormalization()(convblock_4)
  convblock_4_5 = layers.MaxPooling2D(pool_size=2)(convblock_4)

  block_5 = layers.Conv2D(512, 3, activation='relu', padding='same')(convblock_4_5)
  block_5 = layers.BatchNormalization()(block_5)
  block_5 = layers.Conv2D(512, 3, activation='relu', padding='same')(block_5)
  block_5 = layers.BatchNormalization()(block_5)

  deconvblock_4 = layers.UpSampling2D(size=2)(block_5)
  #deconvblock_4 = layers.Conv2DTranspose(256, (3, 3), strides=(1, 1), padding='same', activation='relu')(block_5)
  #deconvblock_4 = layers.Conv2D(256, 3, activation='relu', padding='same')(deconvblock_4)
  #deconvblock_4 = layers.BatchNormalization()(deconvblock_4)
  deconvblock_4 = layers.concatenate([deconvblock_4, convblock_4], axis=3)
  deconvblock_4 = layers.Conv2D(256, 3, activation='relu', padding='same')(deconvblock_4)
  deconvblock_4 = layers.BatchNormalization()(deconvblock_4)
  deconvblock_4 = layers.Conv2D(256, 3, activation='relu', padding='same')(deconvblock_4)
  deconvblock_4 = layers.BatchNormalization()(deconvblock_4)

  deconvblock_3 = layers.UpSampling2D(size=2)(deconvblock_4)
  #deconvblock_3 = layers.Conv2D(128, 3, activation='relu', padding='same')(deconvblock_3)
  #deconvblock_3 = layers.BatchNormalization()(deconvblock_3)
  deconvblock_3 = layers.concatenate([deconvblock_3, convblock_3], axis=3)
  deconvblock_3 = layers.Conv2D(128, 3, activation='relu', padding='same')(deconvblock_3)
  deconvblock_3 = layers.BatchNormalization()(deconvblock_3)
  deconvblock_3 = layers.Conv2D(128, 3, activation='relu', padding='same')(deconvblock_3)
  deconvblock_3 = layers.BatchNormalization()(deconvblock_3)

  deconvblock_2 = layers.UpSampling2D(size=2)(deconvblock_3)
  #deconvblock_2 = layers.Conv2D(64, 3, activation='relu', padding='same')(deconvblock_2)
  #deconvblock_2 = layers.BatchNormalization()(deconvblock_2)
  deconvblock_2 = layers.concatenate([deconvblock_2, convblock_2], axis=3)
  deconvblock_2 = layers.Conv2D(64, 3, activation='relu', padding='same')(deconvblock_2)
  deconvblock_2 = layers.BatchNormalization()(deconvblock_2)
  deconvblock_2 = layers.Conv2D(64, 3, activation='relu', padding='same')(deconvblock_2)
  deconvblock_2 = layers.BatchNormalization()(deconvblock_2)

  deconvblock_1 = layers.UpSampling2D(size=2)(deconvblock_2)
  #deconvblock_1 = layers.Conv2D(32, 3, activation='relu', padding='same')(deconvblock_1)
  #deconvblock_1 = layers.BatchNormalization()(deconvblock_1)
  deconvblock_1 = layers.concatenate([deconvblock_1, convblock_1], axis=3)
  deconvblock_1 = layers.Conv2D(32, 3, activation='relu', padding='same')(deconvblock_1)
  deconvblock_1 = layers.BatchNormalization()(deconvblock_1)
  deconvblock_1 = layers.Conv2D(32, 3, activation='relu', padding='same')(deconvblock_1)
  deconvblock_1 = layers.BatchNormalization()(deconvblock_1)


  output = layers.Conv2D(input_shape[2]/2, 3, activation='sigmoid', padding='same')(deconvblock_1)


  model = models.Model(input, output, name='u_net')
  #display(model.summary())
  return model

import time

from models.gan_models import *
from util.video_manipulation import *

# https://www.tensorflow.org/tutorials/generative/dcgan

bce = tf.keras.losses.BinaryCrossentropy(from_logits=True)
mse = tf.keras.losses.mean_squared_error


def discriminator_loss(real_output, fake_output):
    real_loss = bce(tf.ones_like(real_output), real_output)
    fake_loss = bce(tf.zeros_like(fake_output), fake_output)
    return real_loss + fake_loss


def generator_loss(real_output, fake_output):
    fake_loss = bce(tf.ones_like(fake_output), fake_output)
    return fake_loss


@tf.function
def train_step(inp, images, generator_optimizer, discriminator_optimizer):
    gen_loss, disc_loss = 0, 0
    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
        generated_images = generator(inp, training=True)
        real_output = discriminator(images, training=True)
        fake_output = discriminator(generated_images, training=True)
        gen_loss = generator_loss(real_output, fake_output)
        disc_loss = discriminator_loss(real_output, fake_output)
    gradients_of_generator = gen_tape.gradient(gen_loss, generator.trainable_variables)
    gradients_of_discriminator = disc_tape.gradient(disc_loss, discriminator.trainable_variables)
    generator_optimizer.apply_gradients(zip(gradients_of_generator, generator.trainable_variables))
    discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, discriminator.trainable_variables))
    return gen_loss, disc_loss


def train(epochs, generator_optimizer, discriminator_optimizer, manager):
    for epoch in range(epochs):
        start = time.time()
        gen_loss, disc_loss = 0, 0
        for Xb, yb in batch_split(X, y, 32):
            gl, dl = train_step(Xb, yb, generator_optimizer, discriminator_optimizer)
            gen_loss += gl.numpy()
            disc_loss += dl.numpy()
        cur_ep = epoch + 18038

        if cur_ep % 200 == 0:
            generate_and_save_gif(generator, cur_ep, X)
            display.clear_output()
        if cur_ep % 20 == 0:
            manager.save(checkpoint_number=1)
            generator.save_weights('./resources/kocka_mp4/GAN/generator/gen_4')

        print('Time for epoch', cur_ep, 'is', time.time() - start)
        print('Gen loss:', gen_loss)
        print('Disc loss:', disc_loss)


def generate_and_save_gif(model, epoch, input):
    generated_images = model.predict(input)
    create_gif(generated_images[:, :, :, ::-1], './resources/kocka_mp4/GAN/generated/generated_' + str(epoch) + '.gif')


########################################################################################################################


def train_gan(generator, discriminator, checkpoint_prefix='./resources/kocka_mp4/GAN/gan/gan-2', epochs=500000,
              lr_gen=1e-6,
              lr_dis=1e-6):
    manager, generator_optimizer, discriminator_optimizer = prepare_gan(generator, discriminator, lr_gen, lr_dis,
                                                                        checkpoint_prefix)
    train(epochs, generator_optimizer, discriminator_optimizer, manager)


if __name__ == '__main__':
    frames = load_video('./resources/kocka.mp4')
    X, y = split_frames(frames)
    generator = make_generator_model(X[0].shape)
    generator = load_generator_weights(generator)
    discriminator = make_discriminator_model(y[0].shape)

    train_gan(generator, discriminator)  # trenink siti "oddelene"

    # history_filepath = './resources/kocka_mp4/GAN/generator/hist.csv'
    # train_model(generator, X, y, checkpoint_filepath, history_filepath, SSIM)

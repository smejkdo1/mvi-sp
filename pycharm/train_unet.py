from models.train_model import train_model
from models.unet_model import *
from util.video_manipulation import *

if __name__ == '__main__':
    frames = load_video('./resources/kocka.mp4')
    X, y = split_frames(frames)
    model = U_Net_model(X[0].shape)
    model = load_unet_weights(model)

    checkpoint_filepath = './resources/kocka_mp4/U_Net/u_net_4'
    history_filepath = './resources/kocka_mp4/U_Net/hist.csv'

    train_model(model, X, y, checkpoint_filepath, history_filepath, 'mse', 32)

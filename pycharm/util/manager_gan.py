from models.gan_models import *
from util.video_manipulation import load_video


def create_manager(generator, discriminator, generator_optimizer, discriminator_optimizer, checkpoint_prefix):
    checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
                                     discriminator_optimizer=discriminator_optimizer,
                                     generator=generator,
                                     discriminator=discriminator)
    manager = tf.train.CheckpointManager(checkpoint, directory=checkpoint_prefix, max_to_keep=5)
    manager.restore_or_initialize()
    checkpoint.restore(manager.latest_checkpoint)
    load_checkpoint_filepath = './resources/kocka_mp4/GAN/generator/gen_4'
    generator.load_weights(load_checkpoint_filepath)
    return manager
import cv2
import numpy as np
import imageio
from IPython import display


def resize_vid(width, height, n_pools=4):
    if (width > 300):
        ratio = 256.0 / width
        width = 256
        height *= ratio
    if (height > 300):
        ratio = 256.0 / height
        height = 256
        width *= ratio

    width = int(width)
    height = int(height)
    w = width - (width % np.power(2, n_pools))  # abych mohl vyuzivat pooling a pak zpet do stejneho rozliseni
    h = height - (height % np.power(2, n_pools))
    return w, h


# https://stackoverflow.com/questions/33311153/python-extracting-and-saving-video-frames
def load_video(location):
    vidcap = cv2.VideoCapture(location)
    success, image = vidcap.read()
    width = vidcap.get(3)  # float
    height = vidcap.get(4)  # float
    width, height = resize_vid(width, height)
    frames = []
    while success:
        image = cv2.resize(image, (width, height), fx=0, fy=0, interpolation=cv2.INTER_CUBIC)
        frames.append(image)
        success, image = vidcap.read()
    frames = np.array(frames) / 256.0  # normalize
    return frames


def split_frames(frames):
    X, y = list(), list()
    for i in range(frames.shape[0] - 2):
        X.append(np.concatenate((frames[i], frames[i + 2]), axis=2))
        y.append(frames[i + 1])
    return np.array(X), np.array(y)


def create_gif(frames, filename):
    with imageio.get_writer(filename, mode='I') as writer:
        for frame in frames:
            writer.append_data(frame)
            display.clear_output(wait=True)
        writer.append_data(frame)
    return


def batch_split(X, y, batch_size):
    for i in range(0, X.shape[0], batch_size):
        yield X[i:i + batch_size], y[i:i + batch_size]


def interpolation_input(frames):
    X = list()
    for i in range(frames.shape[0] - 1):
        X.append(np.concatenate((frames[i], frames[i + 1]), axis=2))
    return np.array(X)


def join_frames(output, interpolated_frames):
    result = []
    result.append(output[0])
    for i in range(1, output.shape[0]):
        result.append(interpolated_frames[i - 1])
        result.append(output[i])

    return np.array(result)


# Generuj "zpomalene video"
def interpolate_frames(model, frames, n_times, predict_all=False):
    output = frames.copy()
    X, y = split_frames(frames)

    for i in range(n_times):
        input = interpolation_input(output)
        interpolated_frames = model.predict(input)
        if i == 0 and predict_all:
            output = join_frames(model.predict(X), interpolated_frames)
        else:
            output = join_frames(output, interpolated_frames)
    return output

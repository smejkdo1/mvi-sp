import numpy as np
import tensorflow as tf
#from skimage.metrics import structural_similarity as ssim

from models.unet_model import U_Net_model
from util.video_manipulation import *

ssim = tf.image.ssim_multiscale


def SSIM(y_true, y_pred):
    y_pred = tf.image.rgb_to_grayscale(tf.convert_to_tensor(y_pred))
    y_true = tf.image.rgb_to_grayscale(tf.cast(y_true, y_pred.dtype))
    arr = tf.image.ssim_multiscale(y_pred, y_true, 1)

    # for i in range(y_pred.shape[0]):
    #    image1 = y_true[i]
    #    image2 = y_pred[i]
    #    score, loss = ssim(image1.numpy(), image2.numpy(), data_range=tf.reduce_max(image2).numpy() - tf.reduce_min(image2).numpy(), multichannel=True, full=True)
    #    # print(grad.shape)
    #    if i == 0:
    #        arr = loss.reshape((1, loss.shape[0], loss.shape[1]))
    #    else:
    #        arr = np.concatenate([arr, loss.reshape((1, loss.shape[0], loss.shape[1]))], axis=0)
    #    result.append(1.0 - score)
    return tf.convert_to_tensor(arr)


if __name__ == '__main__':
    frames = load_video('./resources/kocka.mp4')
    X, y = split_frames(frames)
    model = U_Net_model(X[0].shape)

    # print(tf.keras.losses.MSE(y, model.predict(X)))
    print(SSIM(y, model.predict(X)))

import time

from models.gan_models import *
from models.train_model import train_model, get_input_output
from util.video_manipulation import *

# https://www.tensorflow.org/tutorials/generative/dcgan

bce = tf.keras.losses.BinaryCrossentropy()#from_logits=True
mse = tf.keras.losses.mean_squared_error


def discriminator_loss(real_output, fake_output):
    real_loss = bce(tf.ones_like(real_output), real_output)
    fake_loss = bce(tf.zeros_like(fake_output), fake_output)
    return real_loss, fake_loss


def generator_loss(real_output, fake_output):
    fake_loss = bce(tf.ones_like(fake_output), fake_output)
    return fake_loss


@tf.function
def train_step(inp, images, generator_optimizer, discriminator_optimizer):
    gen_loss, disc_loss = 0, 0
    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
        gen_tape.watch(generator.trainable_variables)
        disc_tape.watch(discriminator.trainable_variables)

        generated_images = generator(inp, training=True)
        real_output = discriminator(images, training=True)
        fake_output = discriminator(generated_images, training=True)
        gen_loss = generator_loss(real_output, fake_output)
        real_disc_loss, fake_disc_loss = discriminator_loss(real_output, fake_output)
        disc_loss = real_disc_loss + fake_disc_loss
    gradients_of_generator = gen_tape.gradient(gen_loss, generator.trainable_variables)
    gradients_of_discriminator = disc_tape.gradient(disc_loss, discriminator.trainable_variables)
    generator_optimizer.apply_gradients(zip(gradients_of_generator, generator.trainable_variables))
    discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, discriminator.trainable_variables))
    return gen_loss, real_disc_loss, fake_disc_loss


def train(epochs, generator_optimizer, discriminator_optimizer, manager):
    for epoch in range(epochs):
        start = time.time()
        gen_loss, real_disc_loss, fake_disc_loss = 0, 0, 0
        for Xb, yb in batch_split(X, y, 32):
            gl, rdl, fdl = train_step(Xb, yb, generator_optimizer, discriminator_optimizer)
            gen_loss += gl.numpy()
            real_disc_loss += rdl.numpy()
            fake_disc_loss += fdl.numpy()
        cur_ep = epoch + 4401

        if cur_ep % 200 == 0:
            generate_and_save_gif(generator, cur_ep, X)
        if cur_ep % 20 == 0:
            manager.save(checkpoint_number=1)
            generator.save_weights('./resources/kocka_a_anime/GAN/generator/gen_trained_by_gan') #ukladat samotny generator
        print('Time for epoch', cur_ep, 'is', time.time() - start, '.', 'Gen loss:', gen_loss, 'Real disc loss:', real_disc_loss, 'Fake disc loss:', fake_disc_loss)

def generate_and_save_gif(model, epoch, input):
    generated_images = model.predict(input)
    create_gif(generated_images[:, :, :, ::-1], './resources/kocka_a_anime/GAN/generated/generated_' + str(epoch) + '.gif')


########################################################################################################################

def train_gan(generator, discriminator, checkpoint_prefix='./resources/kocka_a_anime/GAN/gan/gan_1', epochs=500000,
              lr_gen=1e-7,
              lr_dis=1e-6):
    manager, generator_optimizer, discriminator_optimizer = prepare_gan(generator, discriminator, lr_gen, lr_dis,
                                                                        checkpoint_prefix)
    train(epochs, generator_optimizer, discriminator_optimizer, manager)


if __name__ == '__main__':
    X, y = get_input_output()
    generator = make_generator_model(X[0].shape)
    #generator = load_generator_weights(generator)
    discriminator = make_discriminator_model(y[0].shape)
    train_gan(generator, discriminator)  # trenink siti "najednou oddelene"

    #history_filepath = './resources/kocka_a_anime/GAN/generator/hist.csv'
    #checkpoint_filepath = './resources/kocka_a_anime/GAN/generator/gen_1'
    #train_model(generator, X, y, checkpoint_filepath, history_filepath)

from tensorflow.keras import callbacks, optimizers
import pandas as pd
import numpy as np

# pridany parametr val pro volbu trenovani s validaci nebo bez
from util.gif_manipulation import load_gif
from util.video_manipulation import load_video, split_frames


def train_model(model, X, y, checkpoint_filepath, history_filepath, loss='mse', batch_size=64, val=True):
    history = []
    patience = 60
    if val:
        monitor = 'val_loss'
    else:
        monitor = 'loss'
    early_stopping_callback = callbacks.EarlyStopping(monitor=monitor, patience=patience)
    model_checkpoint_callback = callbacks.ModelCheckpoint(
        filepath=checkpoint_filepath,
        save_weights_only=True,
        monitor=monitor,
        mode='min',
        save_best_only=True)
    i = 0
    lrates = [1e-3, 1e-4, 1e-5]
    try:
        while True:
            i = i + 1
            i = i % 3
            optimizer = optimizers.Adam(lr=lrates[i], beta_1=0.9, beta_2=0.999, epsilon=1e-08)
            model.compile(optimizer=optimizer, loss=loss)
            if val:
                epochs = model.fit(X, y, batch_size=batch_size, epochs=100000, verbose=2,
                               callbacks=[model_checkpoint_callback, early_stopping_callback], validation_split=0.2)
            else:
                epochs = model.fit(X, y, batch_size=batch_size, epochs=100000, verbose=2,
                               callbacks=[model_checkpoint_callback, early_stopping_callback])
            history.append(epochs)
            model.load_weights(checkpoint_filepath)
    finally:
        h = history
        try:
            his = pd.read_csv(history_filepath)
        except pd.errors.EmptyDataError:
            his = pd.DataFrame()
        print(his)
        for i in range(len(h)):
            print(h[i].history)
            his = pd.concat([his, pd.DataFrame(h[i].history)], axis=0, ignore_index=True)
        pd.DataFrame(his).to_csv(history_filepath)


def get_input_output():
    frames = load_video('./resources/kocka.mp4')
    gif = load_gif('./resources/anime.gif')
    X1, y1 = split_frames(frames)
    X2, y2 = split_frames(gif)
    X = np.concatenate([X1, X2], axis=0)
    y = np.concatenate([y1, y2], axis=0)
    return X,y
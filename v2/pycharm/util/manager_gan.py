from models.gan_models import *


def create_manager(generator, discriminator, generator_optimizer, discriminator_optimizer, checkpoint_prefix):
    checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
                                     discriminator_optimizer=discriminator_optimizer,
                                     generator=generator,
                                     discriminator=discriminator)
    manager = tf.train.CheckpointManager(checkpoint, directory=checkpoint_prefix, max_to_keep=5)
    manager.restore_or_initialize()
    checkpoint.restore(manager.latest_checkpoint)
    load_gen_checkpoint_filepath = './resources/kocka_a_anime/GAN/generator/gen_1'
    generator.load_weights(load_gen_checkpoint_filepath)
    return manager
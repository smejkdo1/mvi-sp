from models.gan_models import *
from models.unet_model import *
from util.gif_manipulation import load_gif
from util.video_manipulation import *


def solve_arguments(loc, cls):
    if '.gif' in loc:
        frames = load_gif(loc)
    else:
        frames = load_video(loc)
    if 'unet' in cls:
        model = U_Net_model((frames.shape[1], frames.shape[2], frames.shape[3] * 2))
        model = load_unet_weights(model)
    else:
        model = make_generator_model((frames.shape[1], frames.shape[2], frames.shape[3] * 2))
        model = load_generator_weights(model)
    return frames, model

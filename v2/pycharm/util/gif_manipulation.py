from PIL import Image, ImageSequence
import numpy as np

#https://stackoverflow.com/questions/41718892/pillow-resizing-a-gif
def resize_gif(path, n_pools = 4, resize_to=None, save_as=None):
    all_frames = extract_and_resize_frames(path, n_pools, resize_to)

    return all_frames

    if not save_as:
        save_as = path

    if len(all_frames) == 1:
        print("Warning: only 1 frame found")
        all_frames[0].save(save_as, optimize=True)
    else:
        all_frames[0].save(save_as, optimize=True, save_all=True, append_images=all_frames[1:], loop=1000)


def analyseImage(path):
    """
    Pre-process pass over the image to determine the mode (full or additive).
    Necessary as assessing single frames isn't reliable. Need to know the mode
    before processing all frames.
    """
    im = Image.open(path)
    results = {
        'size': im.size,
        'mode': 'full',
    }
    try:
        while True:
            if im.tile:
                tile = im.tile[0]
                update_region = tile[1]
                update_region_dimensions = update_region[2:]
                if update_region_dimensions != im.size:
                    results['mode'] = 'partial'
                    break
            im.seek(im.tell() + 1)
    except EOFError:
        pass
    return results


def extract_and_resize_frames(path, n_pools, resize_to=None):
    """
    Iterate the GIF, extracting each frame and resizing them

    Returns:
        An array of all frames
    """
    mode = analyseImage(path)['mode']

    im = Image.open(path)

    if not resize_to:
        width, height = im.size
        if(width > 256):
          ratio = 256.0/width
          width = 256
          height *= ratio

        if(height > 256):
          ratio = 256.0/height
          height = 256
          width *= ratio #muze zlobit, muze zmenit na float

        w = width - (width % np.power(2,n_pools))
        h = height - (height % np.power(2,n_pools))
        resize_to = (w, h)

    i = 0
    p = im.getpalette()
    last_frame = im.convert('RGBA')

    all_frames = []

    try:
        while True:
            # print("saving %s (%s) frame %d, %s %s" % (path, mode, i, im.size, im.tile))

            '''
            If the GIF uses local colour tables, each frame will have its own palette.
            If not, we need to apply the global palette to the new frame.
            '''
            if not im.getpalette():
                im.putpalette(p)

            new_frame = Image.new('RGBA', im.size)

            '''
            Is this file a "partial"-mode GIF where frames update a region of a different size to the entire image?
            If so, we need to construct the new frame by pasting it on top of the preceding frames.
            '''
            if mode == 'partial':
                new_frame.paste(last_frame)

            new_frame.paste(im, (0, 0), im.convert('RGBA'))



            new_frame.thumbnail(resize_to, Image.ANTIALIAS)
            ##
            (left, upper, right, lower) = (0, 8, 256, 136)
            new_frame = new_frame.crop((left, upper, right, lower)) ##added specifically for anime.gif
            new_frame = new_frame.rotate(90, expand=True)
            ##
            all_frames.append(new_frame)

            i += 1
            last_frame = new_frame
            im.seek(im.tell() + 1)
    except EOFError:
        pass
    return all_frames

def load_gif(path, n_pools = 4):
    tmp = resize_gif(path, n_pools)
    frames = np.array([np.array(frame.copy().convert('RGB').getdata(),dtype=np.uint8).reshape(frame.size[1],frame.size[0],3) for frame in tmp]) /256.0 #normalize
    return frames[:,:,:,::-1] #BGR

from models.train_model import *
from models.unet_model import *

if __name__ == '__main__':
    X, y = get_input_output()
    model = U_Net_model(X[0].shape)
    model = load_unet_weights(model)

    checkpoint_filepath = './resources/kocka_a_anime/U_Net/u_net_4'
    history_filepath = './resources/kocka_a_anime/U_Net/hist.csv'

    train_model(model, X, y, checkpoint_filepath, history_filepath, 32)

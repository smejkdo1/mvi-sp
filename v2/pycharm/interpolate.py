from argparse import ArgumentParser

from util.argument_utilities import solve_arguments
from util.gif_manipulation import load_gif
from util.video_manipulation import *


def interpolate(loc, cls):
    frames, model = solve_arguments(loc, cls)

    interpolated = interpolate_frames(model, frames, 1)
    if 'anime.gif' in loc:
        interpolated = np.rot90(interpolated, -1, (1,2)) #otocit zpet do puvodniho stavu
    create_gif(interpolated[:, :, :, ::-1], loc[:-4] + '_interpolated_by_' + cls + '.gif')


def mimic(loc, cls):
    frames, model = solve_arguments(loc, cls)
    X, y = split_frames(frames)
    gif = model.predict(X)
    if 'anime.gif' in loc:
        gif = np.rot90(gif, -1, (1,2)) #otocit zpet do puvodniho stavu
    create_gif(gif[:, :, :, ::-1], loc[:-4] + '_imitated_by_' + cls + '.gif')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('video_loc', help='path to video location, ex. "./resources/anime.gif"')
    parser.add_argument('mode', help='0 - mimic original, 1 - interpolate')
    parser.add_argument('cls', help='Choose model: gan/unet')
    arg = parser.parse_args()
    if arg.mode == '1':
        interpolate(arg.video_loc, arg.cls)
    elif arg.mode == '0':
        mimic(arg.video_loc, arg.cls)
    print('Success')
